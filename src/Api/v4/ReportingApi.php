<?php

namespace Drupal\google_analytics_reporting_api_integration\Api\v4;

/**
 * Class ReportingApi.
 *
 * @package Drupal\google_analytics_reporting_api_integration\Api\v4
 */
class ReportingApi {

  /**
   * Base path of the credentials without the trailing /.
   *
   * It's recommended to set this to a sub folder of the private file system.
   * E.g. <path/to/private files>/google_analytics_api.
   *
   * @var string
   */
  protected $basePath;

  /**
   * The credentials file (relative to the basePath).
   *
   * Use the developers console and download your service account
   * credentials in JSON format. Place them in the basePath directory.
   *
   * @var string
   */
  protected $keyFile;

  /**
   * The analytics reporting service.
   *
   * @var \Google_Service_AnalyticsReporting
   */
  protected $analytics;

  /**
   * Name of the app.
   *
   * @var string
   */
  protected $appName;

  /**
   * Array of scopes.
   *
   * @var array
   */
  protected $scopes;

  /**
   * Array of metrics.
   *
   * @var \Google_Service_AnalyticsReporting_Metric[]
   */
  protected $metrics;

  /**
   * Array of dimensions.
   *
   * @var \Google_Service_AnalyticsReporting_Dimension[]
   */
  protected $dimensions;

  /**
   * ReportingApi constructor.
   *
   * @param string $basePath
   *   The base path.
   * @param string $keyFile
   *   The path to the key file with analytics credentials.
   *   See: https://developers.google.com/analytics/devguides/reporting/core/v4/quickstart/service-php
   *   about generating this file.
   * @param string $appName
   *   The name of the app.
   * @param array $scopes
   *   An array of scopes.
   */
  public function __construct($basePath, $keyFile, $appName, $scopes) {
    $this->metrics = array();

    $this->basePath = $basePath;
    $this->keyFile = $keyFile;
    $this->appName = $appName;
    $this->scopes = $scopes;

    $this->initAnalytics();

  }

  /**
   * Initialize the reporting API.
   */
  private function initAnalytics() {
    // Create and configure a new client object.
    /** @var \Google_Client $client */
    $client = new \Google_Client();
    $client->setApplicationName($this->appName);
    $client->setAuthConfig($this->basePath . '/' . $this->keyFile);
    $client->setScopes($this->scopes);
    $this->analytics = new \Google_Service_AnalyticsReporting($client);
  }

  /**
   * Add a metric by expressions and alias.
   *
   * @param string $expression
   *   The expression. E.g. "ga:sessions".
   * @param string $alias
   *   The alias.
   */
  public function addMetric($expression, $alias) {
    $metric = new \Google_Service_AnalyticsReporting_Metric();
    $metric->setExpression($expression);
    $metric->setAlias($alias);

    $this->metrics[] = $metric;
  }

  /**
   * Return the metrics.
   *
   * @return \Google_Service_AnalyticsReporting_Metric[]
   *   An array of metrics.
   */
  public function getMetrics() {
    return $this->metrics;
  }

  /**
   * An array of metric objects.
   *
   * @param \Google_Service_AnalyticsReporting_Metric[] $metrics
   *   The array.
   */
  public function setMetrics(array $metrics) {
    $this->metrics = $metrics;
  }

  /**
   * Add a dimension by name.
   *
   * @param string $name
   *   The name. E.g. "ga:pagePath".
   */
  public function addDimension($name) {
    $dimension = new \Google_Service_AnalyticsReporting_Dimension();
    $dimension->setName($name);

    $this->dimensions[] = $dimension;
  }

  /**
   * Return the dimensions.
   *
   * @return \Google_Service_AnalyticsReporting_Dimension[]
   *   An array of dimensions.
   */
  public function getDimensions() {
    return $this->dimensions;
  }

  /**
   * Set the dimensions.
   *
   * @param \Google_Service_AnalyticsReporting_Dimension[] $dimensions
   *   The dimensions.
   */
  public function setDimensions(array $dimensions) {
    $this->dimensions = $dimensions;
  }

  /**
   * Get reports.
   *
   * @param string $viewId
   *   The view id of the required report.
   * @param string $startDate
   *   The starting date. E.g. "7daysAgo", "2016-11-01".
   * @param string $endDate
   *   The ending date. E.g. "today", "2016-11-30".
   *
   * @return \Google_Service_AnalyticsReporting_GetReportsResponse
   *   The reports.
   */
  public function getReport($viewId, $startDate, $endDate) {
    // Create the DateRange object.
    $dateRange = new \Google_Service_AnalyticsReporting_DateRange();
    $dateRange->setStartDate($startDate);
    $dateRange->setEndDate($endDate);

    // Create the ReportRequest object.
    $request = new \Google_Service_AnalyticsReporting_ReportRequest();
    $request->setViewId($viewId);
    $request->setDateRanges($dateRange);
    $request->setMetrics($this->getMetrics());
    $request->setDimensions($this->getDimensions());

    $body = new \Google_Service_AnalyticsReporting_GetReportsRequest();
    $body->setReportRequests(array($request));
    return $this->analytics->reports->batchGet($body);
  }

}
